from django.shortcuts import render, redirect
from django.contrib.auth import login as login_auth, logout as logout_auth
from django.contrib.auth.models import User, auth
from django.contrib.auth.forms import AuthenticationForm
from .forms import MakeUser

# Create your views here.

def home(request):
	return render(request, 'landing.html')

def logout(request):
	if request.method == 'POST':
		logout_auth(request)
	return redirect('landing:home')

def usersign(request):
	form = MakeUser()
	if request.method == 'POST':
		form = MakeUser(request.POST)
		if form.is_valid():
			if not user:
				return render(request, 'signup.html', {'text': 'Please fill the user field'})
			if form.cleaned_data['password']!= form.cleaned_data['konfirmasi_password']:
				return render(request, 'signup.html', {'form':form, 'error_message':'Password tidak sama'})
			if form.cleaned_data['password'] == pform.cleaned_data['konfirmasi_password']:
				if User.objects.filter(user=user).exists():
					return render(request, 'signup.html', {'text': 'Username already exists'})
			else:
				user = User.objects.create_user(username=username, password=password)
				user.save()
				return render(request, 'signup.html', {'user': user})
			return redirect('/home')
	return render(request, 'signup.html', {'form':form})
