from django.urls import path
from .views import home, logout, usersign

app_name = 'landing'

urlpatterns = [
	path('', home, name='home'),
	path('logout/', logout, name='logout'),
	path('signup/', usersign, name='signup'),
	]