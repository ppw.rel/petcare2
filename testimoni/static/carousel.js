const SLICKOPTIONS = {
  dots: true,
  arrows: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 1500,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
}

async function main() {
  const API_URL = "/testimoni/api/testimoni/?format=json"
  // Get Data
  const data = await (await fetch(API_URL)).json()

  const dataHTML = data.map(function (t) {
    return `
      <div class="pesan">
  <p style="font-size: 32px;">Hewan yang dirawat: ${t.hewan}</p>
  <p style="font-size: 28px;">" ${ t.testimoni} "</p>
  <p style="font-size:25px;"><b>${ t.username}</b></p>
</div>
    `;
  })

  $("#carousel").html(dataHTML.join(""));
  $("#carousel").slick(SLICKOPTIONS);
}


$(document).ready(function () {
  var scroll_pos = 0;
  $(document).scroll(function () {
    scroll_pos = $(this).scrollTop();
    if (scroll_pos > 10) {
      $("body").css('background-color', 'white');
      $("body").css('color', 'black');
    } else {
      $("body").css('background-color', 'black');
      $("body").css('color', 'white');
    }
  });


  $('.overlay').delay(4000);
  $('.overlay').slideUp(300);

  $("#test-button").click(function () {
    $(".testimoni-form").get(0).scrollIntoView()
  })

  $("#testimoni-form").submit(async function (e) {
    e.preventDefault();
    const testimoni = $("#testimoni-field").val();
    const username = $("#username-field").val();
    const hewan = $("#hewan-field").val();
    const csrfToken = $("input[name='csrfmiddlewaretoken']").val()
    const data = {
      "testimoni": testimoni,
      "username": username,
      "hewan": hewan,
    };
    console.log(data);
    const response = await fetch("/testimoni/api/testimoni/create", {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
        'X-CSRFToken': csrfToken
      },
      body: JSON.stringify(data)
    });
    const j = await response.json()
    $("#testimoni-form")[0].reset()
    $("#alert-success").toggle();
    $('#carousel').slick('unslick');
    main();
  });

  main();
});

