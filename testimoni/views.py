from django.shortcuts import render
from .forms import *
from .models import *
from rest_framework import generics
from django.contrib.auth.decorators import login_required
from .serializers import TestimoniSerializer


# Create your views here.
@login_required
def view_testimoni(request):
	if (request.method =="POST"):
		instance = TestimoniForm(request.POST)
		if(instance.is_valid()):
			instance.save()


	testimoni_list = Testimoni.objects.all()
	testimoni_form = TestimoniForm()
	

	context= {
		'testi': testimoni_list,
		'form': testimoni_form,
	}

	return render(request,'view_testimoni.html', context)

class TestimoniList(generics.ListAPIView):
	queryset = Testimoni.objects.all()
	serializer_class = TestimoniSerializer
	lookup_field = "testimoni"

class TestimoniCreateList(generics.ListCreateAPIView):
	queryset = Testimoni.objects.all()
	serializer_class = TestimoniSerializer