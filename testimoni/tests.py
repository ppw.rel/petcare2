from django.test import TestCase,Client
from django.urls import resolve
from .views import *
from django.contrib.auth import get_user_model

# Create your tests here.
class Testimoni(TestCase):
    def setUp(self):
        User = get_user_model()
        user = User.objects.create_user('temporary', 'temporary@gmail.com', 'temporary')

    def test_url_testimoni_is_exist(self):
        self.client.login(username='temporary', password='temporary')
        response=self.client.get('/testimoni/')
        self.assertEqual(response.status_code,200)

    def test_testimoni_to_do_list(self):
        self.client.login(username='temporary', password='temporary')
        response=self.client.get('/testimoni/')
        self.assertTemplateUsed(response,'view_testimoni.html')

    def test_testimoni_using_to_do_list(self):
        found=resolve('/testimoni/')
        self.assertEqual(found.func,view_testimoni)