from django import forms
from .models import *


class TestimoniForm(forms.ModelForm):

    class Meta:
        model = Testimoni
        fields = ('username', 'hewan', 'testimoni')
        widgets = {
            'testimoni': forms.TextInput(attrs={
                'placeholder': 'Tulis testimoni kamu disini',
                'name': "testimoni-field",
                'id': "testimoni-field",
                'required': "True",
            }),
            'username':  forms.TextInput(attrs={
                'placeholder': 'Tulis nama kamu disini',
                'name': "username-field",
                'id': "username-field",
                'required': "True",
            }),

            'hewan': forms.TextInput(attrs={
                'placeholder': 'Jenis hewan yang dirawat',
                'name': "hewan-field",
                'id': "hewan-field",
                'required': "True",
            }),
        }
