from django import forms
from .models import Model_form

class PaymentForm(forms.ModelForm):
	class Meta:
		model = Model_form
		fields = ['nama_pemegang_kartu', 'nomor_kartu_debit', 'kadaluarsa','ccv']
		widgets ={
		'kadaluarsa':forms.DateInput(attrs={'class':'form-control form-control-kadaluarsa', 
											'type':'date'}),
											# 'id':'kadaluarsa_input'}),
		'nomor_kartu_debit':forms.NumberInput(attrs={'class':'form-control form-control-nomor', 
											'placeholder':"Masukkan nomor kartu debit/kredit Anda"}),
											# 'id':'nomor_input'}),
		'nama_pemegang_kartu':forms.TextInput(attrs={'class':'form-control form-control-nama', 
											'placeholder':'Masukkan nama yang tertera di kartu Anda'}),
											# 'id':'nama_input'}),
		'ccv':forms.NumberInput(attrs={'class':'form-control form-control-ccv',
										'placeholder':'Masukkan 3-4 digit terakhir dari nomor kartu Anda'}),
											# 'id':'ccv_input'})
		} 