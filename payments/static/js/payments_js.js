$(document).ready(function(){
    $("#button_konfirmasi").prop('disabled', true);
    var toValidate = $('#nama_field input, #nomor_kartu_field input, #kadaluarsa_field input, #ccv_field input');
    toValidate.keyup(function () {
        var valid = true;
        $.each(toValidate, function (index, item) {
            if($(item).val() === "") {
                valid = false;
            }
        });
        if (valid) {
            console.log("valid")
            $("#button_konfirmasi").prop('disabled', false);
        }else {
            $("#button_konfirmasi").prop('disabled', true);
        }
    });
    $('#form').submit(function(e) {
        var nama = $('#nama_field input').val();
        var nomor_kartu = $('#nomor_kartu_field input').val();
        var kadaluarsa = $('#kadaluarsa_field input').val();
        var ccv = $('#ccv_field input').val();
     
        $(".error").remove();
        console.log(nomor_kartu.length)
        if (nama.length > 50) {
            $('#nama_field').after('<span class="error col-lg-3 ">Input is invalid</span>');
            event.preventDefault();
        }
        if (nomor_kartu.length<15 || nomor_kartu.length>16) {
            $('#nomor_kartu_field').after('<span class="error col-lg-3">Input harus 15 digit atau 16 digit</span>');
            event.preventDefault();
        }
        if (ccv.length<3 || ccv.length> 4) {
            $('#ccv_field').after('<span class="error col-lg-3">Input harus 3-4 digit terakhir</span>');
            event.preventDefault();
        }
        if(ccv.length==3){
            if(ccv != nomor_kartu%1000){
                $('#ccv_field').after('<span class="error col-lg-3">Input harus 3-4 digit terakhir yang sama</span>');
                event.preventDefault();
            }
        }
        if(ccv.length==4){
            if(ccv != nomor_kartu%10000){
                $('#ccv_field').after('<span class="error col-lg-3">Input harus 3-4 digit terakhir yang sama</span>');
                event.preventDefault();
            }
        }
    });
    $("#nama_field").hover(function(){
        $(".form-control-nama").css("border", "2px solid #537054");
    }, function(){
        $(".form-control-nama").css("border", "1px solid #ced4da");
    });
    $("#nomor_kartu_field").hover(function(){
        $(".form-control-nomor").css("border", "2px solid #537054");
    }, function(){
        $(".form-control-nomor").css("border", "1px solid #ced4da");
    });
    $("#ccv_field").hover(function(){
        $(".form-control-ccv").css("border", "2px solid #537054");
    }, function(){
        $(".form-control-ccv").css("border", "1px solid #ced4da");
    });
    $("#kadaluarsa_field").hover(function(){
        $(".form-control-kadaluarsa").css("border", "2px solid #537054");
    }, function(){
        $(".form-control-kadaluarsa").css("border", "1px solid #ced4da");
    });
});