"Pet Care"

Nama Anggota :
1. Karin Patricia membuat landing page
2. Marcel Valdano membuat testimoni page
3. Sinta Bela membuat isiform pemesanan salon
4. Siti Khadijah membuat halaman konfirmasi pembayaran

Link Herokuapp :
http://petcarepw.herokuapp.com/

Ide Cerita:
Pet Care merupakan sebuah tempat salon hewan yang menyediakan layanan untuk hewan peliharaan. 
Pembuatan pesanan akan dilayani dalam website Pet Care beserta dengan alur pembayarannya.
Kami berharap adanya website ini akan memudahkan para pemilik memanjakan hewan peliharaannya dengan fasilitas yang kami sediakan.

Daftar fitur:
1. Home Page
2. Frequently Asked Questions
3. Pemesanan Layanan
4. Cara pembayaran
5. Testimoni
6. Pesan

[![pipeline status](https://gitlab.com/ppw.rel/pet-care/badges/master/pipeline.svg)]