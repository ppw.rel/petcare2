from django.apps import AppConfig


class IsiformappConfig(AppConfig):
    name = 'isiFormApp'
