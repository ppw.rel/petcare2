from django.urls import path, include

from .views import *
app_name = 'isiFormApp'

urlpatterns = [
	path('', isiForm, name='isiForm'),
	path('create_post', create_post, name='create_post'),
	path('getAllData', getAllData, name='getAllData'),

]